import path from "path";
import HtmlWebpackPlugin from "html-webpack-plugin";
import MiniCssExtractPlugin from "mini-css-extract-plugin";

const config = {
    devtool: "source-map",
    mode: "development",
    plugins: [
        new HtmlWebpackPlugin({template: './src/html/index.html'}),
        new MiniCssExtractPlugin()
    ],
    devServer: {
        hot: false,
        static: {
            directory: path.resolve("dist")
        },
        open: true,
    },
    resolve: {
        extensions: [".ts", ".js"],
        extensionAlias: {'.js': ['.js', '.ts']}
    },
    module: {
        rules: [
            {
                test: /\.ts$/i,
                use: ["ts-loader"],
                exclude: /node_modules/
            },
            {
                test: /\.css$/i,
                use: [MiniCssExtractPlugin.loader, "css-loader"]
            },
            {test: /\.html?$/i, use: ['html-loader']},
            {test: /\.(png|svg|jpg|jpeg|gif|webp)$/i, type: "asset"},
            {test: /\.(woff2?|eot|ttf|otf)$/i, type: "asset"}

        ]
    },
    output: {
        clean: true
    },
};

export default config;